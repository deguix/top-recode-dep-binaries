# top-recode-dep-binaries

This project contains binaries needed by top-recode so one wouldn't need to compile them and spend several hours doing so (specially openssl). This is meant to be pulled by the main project.